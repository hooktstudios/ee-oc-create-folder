<?php
/**
 * @author Hookt Studios inc. <info@hooktstudios.com>
 * @copyright Hookt Studios inc. 2015
 */

namespace OCA\CreateFolder\Controller;


use \OCP\IRequest;
use \OCP\AppFramework\ApiController;
use \OCP\AppFramework\Http\JSONResponse;
use \OCP\AppFramework\Http;

class CreateFolderController extends ApiController {

	private $userId;
	private $folder;

	public function __construct($AppName, IRequest $request, $UserId) {
		parent::__construct($AppName, $request);
		$this->userId = $UserId;
		$this->folder = \OC::$server->getUserFolder();
	}

	// This route is protected by admin by default
	/**
	 * @NoCSRFRequired
	 */
	public function create() {
		$parent = $this->request->getParam('parent');
		$path = $this->request->getParam('path');
		$emptyFile = $this->request->getParam('emptyFile');


		if(!$parent || !$path) {
			return new JSONResponse([
				'error' => 'path and parent params are required'
			], 422);
		}

		$fullpath = $parent . DIRECTORY_SEPARATOR . $path;

		if($this->folder->nodeExists($fullpath)) {
			return new JSONResponse([
				'status' => 'EXISTS',
				'path' => $fullpath,
			]);
		}
		

		$newFolder = $this->folder->newFolder($fullpath);
		if($emptyFile) {
			str_replace(array('/', '\\', ':', '?', '*', '<', ':', '|'), '', $emptyFile);
			$newFolder->newFile($emptyFile);
		}

		return new JSONResponse([
			'status' => 'CREATED',
			'path' => $fullpath,
		]);
	}
}
