# Extension `Create Folder`

## Installation:

- Déposez l'extension dans `owncloud/apps/create_folder`
- Installez l'application avec un compte administrateur ownCloud dans le menu d'aministration


## API

Tous les appels doivent être authentifiés avec Auth Basic. La création de
dossier se fait pour un utilisateur spécifiquement. *Cet API a été pensé pour
être utiliser avec le compte maître de l'installation OwnCloud*, compte qui
contiendra les dossiers à partager aux autres utilisateurs.

### Endpoints

`POST /index.php/apps/create_folder/create?path=__new_folder_to_create__&parent=__where_to_create_it__`

Eg: `POST /index.php/apps/create_folder/create?path=3013 - John Smith&parent=Clients`
va va créer le dossier `Clients/3013 - John Smith`


## Configuration

Voici quelque prérequis pour que l'app puisse remplir le besoin d'affaire:

- Créer un compte maître administrateur (ex: Envolee), lui donner un mot de
  passe long et **ne jamais le changer**. Ce mot de passe devra être inscrit
  dans l'application de facturation.
- Créer un groupe "Employés" duquel tous les employés feront partie.
- Créer un dossier `Clients` sous ce compte maître. Partager ce dossier au
  groupe "Employés" avec les permissions désirées.


## Notes d'utilisation

- Ne pas changer le mot de passe du compte maitre sans aussi le changer dans
  l'appliation de facturation
- Ne pas renommer les noms des dossiers clients, sinon l'application de
  facturation ne les trouvera plus
