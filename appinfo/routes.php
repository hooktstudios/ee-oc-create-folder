<?php
/**
 * @author Hookt Studios inc. <info@hooktstudios.com>
 * @copyright Hookt Studios inc. 2015
 */


return array(
    'routes' => array(
        array('name' => 'create_folder#create', 'url' => '/create', 'verb' => 'POST'),
    )
);
