<?php
/**
 * @author Hookt Studios inc. <info@hooktstudios.com>
 * @copyright Hookt Studios inc. 2015
 */

namespace OCA\CreateFolder\AppInfo;


require('apps/create_folder/appinfo/application.php');
require('apps/create_folder/lib/controller/createfoldercontroller.php');
$container = new Application();
