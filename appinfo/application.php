<?php
namespace OCA\CreateFolder\AppInfo;

use \OCP\AppFramework\App;

use \OCA\CreateFolder\Controller\CreateFolder;

class Application extends App {

	public function __construct(array $urlParams=array()) {
		parent::__construct('create_folder', $urlParams);

		// Not required, but it'd be great to configure the controller properly
		//$container = $this->getContainer();
		//$container->registerService('CreateFolderController', function($c) {
			//return new CreateFolderController(
				//// This never gets called
				//'create_folder',
				//$c->query('Request'),
				//$c->query('ServerContainer')->getUserFolder(),
				//$c->query('UserId')
			//);
		//});
	}
}
